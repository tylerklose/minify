# Ruby Minifier

Rub Minifier is a tool that reduces the filesize of your Ruby code. This tool is being created to help push along the Serverless movement. 
> Fast functions are small in overall size and scope and don’t “require” many files.

Minifying Ruby files will assist in ensuring "Faast functions are small in overall size" is respected.

Rather than rolling out a minifier from scratch, it occurred to me that we could leverage the `rubocop` gem. The minifier will pull in Rubocop, disabled all cops, then selectively turn on cops that bring us closer to the goal of minified Ruby. The cops for extra whitespace, unnecessary `return` statements, unneeded require statements.

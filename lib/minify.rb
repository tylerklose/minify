class Minify
  def self.run(file_name)
    file = File.open("path-to-file.tar.gz", "rb")
    contents = file.read

    [
      { name: 'Remove block comments', regex: /=begin[\s\S]*=end/, replacement: '' },
      { name: 'Remove single line comments', regex: /[^"\n]*#[^\{{1}][^"\n]*/, replacement: "" },
      { name: 'Remove newlines', regex: /^\n\s*/, replacement: "" },
      { name: 'Remove leading/trailing whitspace', regex: /(^\s*|\s*$)/, replacement: "" }
    ].each do |m|
      contents = contents.gsub(m[:regex], m[:replacement])
    end

    puts contents
  end
end
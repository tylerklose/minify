#!/bin/ruby

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), "lib"))
require 'minify'

Gem::Specification.new do |s|
  s.name = "minify"
  s.version = Minify::VERSION
  s.author = "c00lryguy"
  s.email = "c00lryguy@gmail.com"
  s.homepage = "http://github.com/c00lryguy/minify"
  s.description = "Minify your files based on their filetype."
  s.summary = "Minify JS, CSS, HTML, ..."
  s.extra_rdoc_files = ["README.md"]
  s.require_paths = ["lib"]
  s.files = Dir['**/*']
end


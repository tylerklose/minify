class Minify
  class Parser
  
    register 'text/plain' do |input|
      input.trim.strip
    end
    
    register 'text/html' do |input|
      p "YAY"
      p input
    
      case use('tidy-ext', 'tidy-fork', 'tidy_ffi', 'tidy')
      when 'tidy' || 'tidy-fork' || 'tidy-ext'
        Tidy.path = '/usr/lib/tidylib.so'
        Tidy.open(:show_warnings=>true) do |tidy|
          puts tidy.options.show_warnings
          tidy.clean(input)
        end
      when 'tidy_ffi'
        TidyFFI::Tidy.new(input).clean
      end
    end
    
    register 'text/css' do |input|
      case use('cssmin')
      when 'cssmin'
        CSSMin.minify(input)
      end
    end
    
    register 'application/javascript' do |input|
      case use('jsmin_c', 'Jsmin', 'jsmin_ffi', 'jsmin')
      when 'jsmin_c'
        JS.new.minimize(input)
      when 'jsmin' || 'Jsmin'
        JS.minify(input)
      when 'jsmin_ffi'
        JS.minify!(input)
      end
    end
    
  end
end

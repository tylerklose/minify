class Minify
  class Parser
    class << self
      include MetaTools
      
      attr_reader :index
      
      def register(mime_type, &blk)
        mime_type = MIME::Types[mime_type].first unless mime_type.is_a?(MIME::Type)
        meth = mime_type.sub_type.gsub(/-/, "_").to_sym
        (@index ||= {})[mime_type.to_s] = meth
        meta_def(meth, &blk)
      end # Parser.register
      
      def call(mime_type, input)
        send(@index[mime_type], input)
      end # Parser.call
      
      #---
      # HELPER METHODS
      #+++
      
      # Try to require multiple libraries and return the first library that exists
      # and require it
      def use(*libs)
        lib = libs.find do |lib|
          result = begin
            r = require(lib)
            puts "r: #{r}"
            true
          rescue LoadError
            false
          end
          puts "result: #{result}"
          result
        end
        puts "lib: #{lib}"
        raise(LoadError, "no such files to load -- #{libs.join(", ")}") if lib.nil?
        
        lib
      end # Parser.use
      
    end # << self
  end # Parser
end

# This is a comment that should be removed

=begin
  Block comments
  should also
  be removed
=end

# The whitespace between these comments should be removed

# Class and method names should be shortened
class TylerKlose
  # The commas between attributes can be removed
  attr_accessor :name, :twitter, :employer, :industry, :location

  # The unnecessary whitespace at the end of `def initialize` should also go away
  def initialize        
    @name = 'Tyler Klose'
    @twitter = '@tylerklose'
    @employer = 'Single Stop'
    @industry = 'Technology, Non-profit'
    @location = {
      city: 'New York',
      state: 'NY'
    }
  end

  # Remove unnecessary return
  def city_state
    return "#{@location[:city]}, #{@location[:state]}"
  end

  def uncalled_function
    if @name == 'Tyler Klose'
      signature = 'tk.'
    else
      signature = 'anon.'
    end

    ['I', 'am', 'never', 'called'].each do |word|
      puts word
    end

    puts signature
  end
end
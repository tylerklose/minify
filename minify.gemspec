Gem::Specification.new do |s|
  s.name        = 'minify'
  s.version     = '0.0.0'
  s.date        = '2019-05-04'
  s.summary     = "Minify your Ruby files"
  s.description = "Minify your Ruby files"
  s.authors     = ["Tyler Klose"]
  s.email       = 'tylerklose@gmail.com'
  s.files       = ["lib/minify.rb"]
  # s.homepage    =
  #   'http://rubygems.org/gems/hola'
  s.license       = 'MIT'
end